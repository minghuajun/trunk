/*************************************************************************
*  2012-2020 Václav Šmilauer                                             *
*  2020      Janek Kozicki                                               *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

// functions defined in the respective .cpp files
void expose_matrices1();
void expose_matrices2();
void expose_vectors1();
void expose_vectors2();
void expose_boxes();
void expose_quaternion();
void expose_complex1(); // does nothing if _COMPLEX_SUPPORT is not #defined
void expose_complex2(); // does nothing if _COMPLEX_SUPPORT is not #defined
void expose_converters();

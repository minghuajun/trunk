# encoding: utf-8

# import yade modules that we will use below
from yade import pack, plot


# PSD given as points of piecewise-linear function
psdSizes, psdCumm = [0.02, 0.04, 0.045, 0.05, 0.06, 0.08, 0.12], [0, 0.1, 0.3, 0.3, 0.3, 0.7, 1]


# create rectangular box from facets
O.bodies.append(geom.facetBox((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), wallMask=31))

# create empty sphere packing, it contains only the pure geometry
sp = pack.SpherePack()
# create a random cloud of particles enclosed in a parallelepiped
# the resulting packing is a gas-like state with no contacts
sp.makeCloud((0, 0, 0), (1, 1, 1), psdSizes=psdSizes, psdCumm=psdCumm, num=1000, distributeMass=False)
# add the sphere pack to the simulation
sp.toSimulation()

O.engines = [
    ForceResetter(),
    InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Facet_Aabb()]),
    InteractionLoop(
        [Ig2_Sphere_Sphere_ScGeom(), Ig2_Facet_Sphere_ScGeom()],
        [Ip2_FrictMat_FrictMat_FrictPhys()],
        [Law2_ScGeom_FrictPhys_CundallStrack()]
    ),
    NewtonIntegrator(gravity=(0, 0, -9.81), damping=0.4),
    # call the checkUnbalanced function every 2 seconds
    PyRunner(command='checkUnbalanced()', realPeriod=2)
]

O.dt = 0.1 * PWaveTimeStep()

# if the unbalanced forces goes below 0.05
# the packing is considered stabilized
# stop
def checkUnbalanced():
    if unbalancedForce() < 0.05:
        O.pause()
        # plot.saveDataTxt('bbb.txt.bz2')
        # if it ends with .bz2/.gz, the file will be compressed using bzip2/gzip
        # this format is suitable for being loaded for further processing with numpy.genfromtxt function


O.saveTmp()
